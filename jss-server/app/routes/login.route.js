const loginController = require("../controllers/login.controller.js");

module.exports = function (app, router) {
    app.route('/login')
        .post(loginController.login)
        
}