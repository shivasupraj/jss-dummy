import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, Router, NavigationStart, NavigationEnd, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from  '@angular/common/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LaddaModule } from 'angular2-ladda';

import { TokenInterceptor } from './auth/interceptors/token.interceptor';
import { DimentionalService } from './services/dimentional.service';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { NewReportComponent } from './new-report/new-report.component';
import { TopbarComponent } from './topbar/topbar.component';
import { OnTimeDeliveryFormComponent } from './modals/on-time-delivery-form/on-time-delivery-form.component';
import { ProductivityKpiFormComponent } from './modals/productivity-kpi-form/productivity-kpi-form.component';
import { QualityFormComponent } from './modals/quality-form/quality-form.component';
import { SupplierPpmComponent } from './modals/supplier-ppm/supplier-ppm.component';
import { DeleteConfirmationComponent } from './modals/delete-confirmation/delete-confirmation.component';

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'records/:year/:month', component: NewReportComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    NewReportComponent,
    TopbarComponent,
    OnTimeDeliveryFormComponent,
    ProductivityKpiFormComponent,
    QualityFormComponent,
    SupplierPpmComponent,
    DeleteConfirmationComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    NgbModule,
    HttpClientModule,
    LaddaModule.forRoot({
      style: "expand-right",
      spinnerLines: 12
    })
  ],
  entryComponents: [
    OnTimeDeliveryFormComponent,
    ProductivityKpiFormComponent,
    QualityFormComponent,
    SupplierPpmComponent,
    DeleteConfirmationComponent
  ],
  providers: [
    DimentionalService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
