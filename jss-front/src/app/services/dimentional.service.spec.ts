import { TestBed, inject } from '@angular/core/testing';

import { DimentionalService } from './dimentional.service';

describe('DimentionalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DimentionalService]
    });
  });

  it('should be created', inject([DimentionalService], (service: DimentionalService) => {
    expect(service).toBeTruthy();
  }));
});
