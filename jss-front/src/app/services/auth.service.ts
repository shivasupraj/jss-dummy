import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  checkLoginStatus(){
    var user = JSON.parse(localStorage.getItem('user'));
    if(user && user.token){return true}
    else{return false};
  }

  getLoggedInUser(){
    return JSON.parse(localStorage.getItem('user'));
  }

  login(username, password){
    var payload = {
      "username":username,
      "password":password
    }
    return this.http.post(this.baseUrl + `login`, payload);
  }

  logout(){
    localStorage.removeItem('user');
    localStorage.removeItem('plantId');
  }
}
