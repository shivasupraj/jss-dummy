import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class DimentionalService {
  baseUrl = environment.baseUrl;
  products;
  subProducts;
  plants;
  customers;
  benchmarks;

  benchArray = {};
  productLookup = {};
  subProductLookup = {};
  customerLookup = {};

  index;

  createdDate;
  plantId;
  regionName;
  plantName;
  userId = JSON.parse(localStorage.getItem('user')).User_ID;

  safetyPrameters = {
    Plant_ID: null,
    Benchmark_ID: null,
    User_ID: null,
    Tot_Emp_First_Day_Mon: null,
    Tot_Emp_Last_Day_Mon: null,
    Tot_Lost_Time_Days_Mon: null,
    Lost_Time_Incident: null,
    Created_Date: null,
    Updated_Date: null
  };

  customerData = [];
  supplierData = [];
  onTimeDeliveryData = [];
  productivityData = [];

  private headers = new HttpHeaders({'Content-Type': 'application/json'});


  constructor(private http: HttpClient) {
    this.getProducts().subscribe((result) => {
      //get the results and filter subproducts and products
      //trim these of the Char whitespace
      var arrayedProducts = _.map(result, (x) => {
        this.productLookup[this.trimmer(x, 'Prod_ID')] = this.trimmer(x, 'Prod_Type')
        return this.trimmer(x, 'Prod_Type') + " - " + this.trimmer(x, 'Prod_ID')
      });
      this.products = _.uniq(arrayedProducts);

      this.getSubProducts().subscribe((result) => {
        var arrayedSubProducts = _.map(result, (x) => {
          this.subProductLookup[this.trimmer(x, 'Sub_Prod_ID')] = this.trimmer(x, 'Sub_Prod_Type')
          return this.trimmer(x, 'Sub_Prod_Type') + " - " + this.trimmer(x, 'Sub_Prod_ID')
        })
        this.subProducts = arrayedSubProducts

        this.getCustomers().subscribe((result) => {
          this.customers = _.map(result, (x) => {
            this.customerLookup[this.trimmer(x, 'Cust_ID')] = this.trimmer(x, 'Cust_Name')
            return this.trimmer(x, 'Cust_Name') + " - " + this.trimmer(x, 'Cust_ID')
            /* return {"name":this.trimmer(x,'Cust_Name'),
                    "id":this.trimmer(x, 'Cust_ID')
            } */
          })

          this.getBenchmarks().subscribe((result) => {
            this.benchmarks = _.map(result, (x) => {
              this.benchArray[this.trimmer(x, 'Benchmark_Name')] = this.trimmer(x, 'Benchmark_ID')
            })
          })
        })
      })
    });
  }

  getIDFromString(str) {
    return str.split('- ')[1]
  }

  getProducts() {
    return this.http.get(this.baseUrl + `products`);
    //, post, {headers: this.headers}
  }

  getSubProducts() {
    return this.http.get(this.baseUrl + `subproducts`);
    //, post, {headers: this.headers}
  }

  getCustomers() {
    return this.http.get(this.baseUrl + `customers`);
  }

  getBenchmarks() {
    return this.http.get(this.baseUrl + `benchmarks`);
  }

  getPlants() {
    return this.http.get(this.baseUrl + `plants`);
  }

 /*  getRecordsHistory() {
    return this.http.get(this.baseUrl + `plants`);
  } */

  getReportsPresentForYear(year,plantId) {
    return this.http.get(this.baseUrl + `checkreports?year=` + year +`&plantId=`+plantId);
  }

  getReportsForMonth(date,plantId) {
    return this.http.get(this.baseUrl + `report?date=` + date +`&plantId=`+plantId);
  }
  /** Adding the record to the database */
  addPlantReport(rowData){
    return this.http.post(this.baseUrl + `plants`, rowData, {headers: this.headers, responseType: 'text'});
  }

  addCustomerReport(rowData){
    return this.http.post(this.baseUrl + `customers`, rowData, {headers: this.headers, responseType: 'text'});
  }

  addProductReport(rowData){
    return this.http.post(this.baseUrl + `products`, rowData, {headers: this.headers, responseType: 'text'});
  }
  /** Updating the record to database */
  updatePlantReport(rowData){
    return this.http.put(this.baseUrl + `plants`, rowData, {headers: this.headers, responseType: 'text'});
  }

  updateCustomerReport(rowData){
    return this.http.put(this.baseUrl + `customers`, rowData, {headers: this.headers, responseType: 'text'});
  }

  updateProductReport(rowData){
    return this.http.put(this.baseUrl + `products`, rowData, {headers: this.headers, responseType: 'text'});
  }

  deleteRecord(date, plantId){
    ///delete?date=2019-08-06&plantId=P003
    return this.http.delete(this.baseUrl + `delete?date=` + date + `&plantId=`+plantId,{headers: this.headers, responseType: 'text'});
  }

  trimmer(elem, key) {
    return elem[key].trim();
  }

  resetFields(){
    this.safetyPrameters = {
      Plant_ID: null,
      Benchmark_ID: null,
      User_ID: null,
      Tot_Emp_First_Day_Mon: null,
      Tot_Emp_Last_Day_Mon: null,
      Tot_Lost_Time_Days_Mon: null,
      Lost_Time_Incident: null,
      Created_Date: null,
      Updated_Date: null
    };

    this.customerData = [];
    this.supplierData = [];
    this.onTimeDeliveryData = [];
    this.productivityData = [];

  }
}
