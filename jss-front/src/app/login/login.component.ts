import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../services/auth.service';
import { DimentionalService } from '../services/dimentional.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  username = null;
  password = null;
  attemptingLogin = false;
  failedLogin = false;


  constructor(private authService: AuthService, private router: Router) {
    this.authService.logout();
   }

  ngOnInit() {
  }

  attemptLogin() {
    this.attemptingLogin = true;
    this.authService.login(this.username, this.password).subscribe((returnPayload) => {
      console.log("returnPayload", returnPayload);
      this.attemptingLogin = false;
      localStorage.setItem('user', JSON.stringify(returnPayload));
      this.router.navigate(['']);
    }, (err) => {
      this.attemptingLogin = false;
      this.failedLogin = true;
      this.username = null;
      this.password = null;
      setTimeout(() => {
        this.failedLogin = false;
      }, 5000)
    })
  }

  keyDownFunction(event) {
    if (event.keyCode == 13) {
      this.attemptLogin();
    }
  }

}
