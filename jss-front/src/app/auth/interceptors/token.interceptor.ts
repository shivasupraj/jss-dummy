import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor() {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(request.url.includes('login')){
      return next.handle(request);
    }else{
      var user = JSON.parse(localStorage.getItem('user'));
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ` + user.token
        }
      });
      return next.handle(request);
    }
  }
}
