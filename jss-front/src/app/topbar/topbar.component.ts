import { Component, OnInit } from '@angular/core';

import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.less']
})
export class TopbarComponent implements OnInit {
  user;
  constructor(private authService: AuthService) {
    this.user = this.authService.getLoggedInUser();
  }

  ngOnInit() {
  }

  signOut(){
    this.authService.logout();
  }
}
