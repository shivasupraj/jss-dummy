import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import * as moment from 'moment';

import { DimentionalService } from '../../services/dimentional.service';

@Component({
  selector: 'app-delete-confirmation',
  templateUrl: './delete-confirmation.component.html',
  styleUrls: ['./delete-confirmation.component.less']
})
export class DeleteConfirmationComponent implements OnInit {
  @Input() date: string;
  deleting = false;

  constructor(private activatedRoute: ActivatedRoute,private router:Router, private dimentionalService:DimentionalService, public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  confirmDelete(){
    this.deleting = true;
    var date = new Date(this.date).toISOString();
    var plantId = localStorage.getItem('plantId');
    console.log(plantId);
    this.dimentionalService.deleteRecord(date.substring(0, 10), plantId).subscribe((result:any)=>{
      console.log(result);
      this.deleting=false;
      this.router.navigate(['']);
    })
  }

}
