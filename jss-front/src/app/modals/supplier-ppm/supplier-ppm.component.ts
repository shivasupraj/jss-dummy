import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbActiveModal, NgbModal, NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subject, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';

import { DimentionalService } from '../../services/dimentional.service';

@Component({
  selector: 'app-supplier-ppm',
  templateUrl: './supplier-ppm.component.html',
  styleUrls: ['./supplier-ppm.component.less']
})
export class SupplierPpmComponent implements OnInit {
  
  constructor(public activeModal: NgbActiveModal, private dimentionalService: DimentionalService) {
    this.loadData(this.dimentionalService.index);
   }

  ngOnInit() {
  }
  saving = false;
  supplierPPMPayload = { 
    Plant_ID:null,
    Benchmark_ID:null,
    User_ID:null,
    Prod_ID:null,
    Emp_Left_Mon:null,
    Tot_Emp_First_Day_Mon:null,
    Tot_Emp_Last_Day_Mon:null,
    Tot_Supp_PPM_Qlty:null,
    Cost_Premium_Freight_In_Mon:null,
    Cost_Premium_Freight_Out_Mon:null,
    Revenue_Mon:null,
    Cogs:null,
    Mon_End_Inv_Local_Curr:null,
    Pre_Mon_End_Inv_Local_Curr:null,
    All_Mfg_Cost_PnL:null,
    Overtime_Cost_PnL:null,
    Created_Date:null,
    Updated_Date:null
    /* product: null,
    supplierPPM: null */
  }

  @ViewChild('instance') instance: NgbTypeahead;
  focusProduct$ = new Subject<string>();
  clickProduct$ = new Subject<string>();

  searchProducts = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.clickProduct$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focusProduct$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? this.dimentionalService.products
        : this.dimentionalService.products.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }

  addSupplierPPMRecord() {
    this.saving = true;
    this.supplierPPMPayload.Plant_ID = localStorage.getItem('plantId');
    this.supplierPPMPayload.Benchmark_ID = this.dimentionalService.benchArray["Quality"]
    this.supplierPPMPayload.Prod_ID = this.dimentionalService.getIDFromString(this.supplierPPMPayload.Prod_ID)
    this.supplierPPMPayload.User_ID = JSON.parse(localStorage.getItem('user')).User_ID;
    this.supplierPPMPayload.Created_Date = this.dimentionalService.createdDate;
    this.dimentionalService.supplierData.push(this.supplierPPMPayload);
    setTimeout(() => {
      this.activeModal.dismiss('Timeout');
    }, 1000)
  }

  /** WET code - see the addSupplierPPMRecord and change both addQualityRecord and updateSupplierPPMRecord */
  updateSupplierPPMRecord() {
    this.saving = true;
    this.supplierPPMPayload.Plant_ID = localStorage.getItem('plantId');
    this.supplierPPMPayload.Benchmark_ID = this.dimentionalService.benchArray["Quality"]
    this.supplierPPMPayload.Prod_ID = this.dimentionalService.getIDFromString(this.supplierPPMPayload.Prod_ID)
    this.supplierPPMPayload.User_ID = JSON.parse(localStorage.getItem('user')).User_ID;
    this.supplierPPMPayload.Created_Date = this.dimentionalService.createdDate;
    this.dimentionalService.supplierData[0] = this.supplierPPMPayload;
    setTimeout(() => {
      this.activeModal.dismiss('Timeout');
    }, 1000)
  }

loadData(index): any {
    if (index == -1){
      return;
    }
    console.log(index); 
    let supplierReport = this.dimentionalService.supplierData[index];
    this.supplierPPMPayload.Prod_ID = this.dimentionalService.productLookup[supplierReport.Prod_ID];
    this.supplierPPMPayload.Tot_Supp_PPM_Qlty = supplierReport.Tot_Supp_PPM_Qlty;
  }
}
