import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbActiveModal, NgbModal, NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subject, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';

import { DimentionalService } from '../../services/dimentional.service';
import * as moment from 'moment';

@Component({
  selector: 'app-quality-form',
  templateUrl: './quality-form.component.html',
  styleUrls: ['./quality-form.component.less']
})
export class QualityFormComponent implements OnInit {

  constructor(public activeModal: NgbActiveModal, private dimentionalService: DimentionalService) { 
    this.loadData(this.dimentionalService.index);
  }

  ngOnInit() {
  }

  saving = false;
  qualityTablePayload = {
    Plant_ID: null,
    Benchmark_ID: null,
    Prod_ID: null,
    Sub_Prod_ID: null,
    Cust_ID: null,
    User_ID: null,
    Cnf_Prod_Shipped_Mon: null,
    Tot_Prod_Shipped_Mon: null,
    Revenue_Mon: null,
    Tot_Rjctd_Qty_For_Mon: null,
    Shipped_Qty_For_Mon:null,
    Scrap_For_Mon:null,
    No_Of_Complaints:null,
    Created_Date:null,
    Updated_Date:null
  }

  @ViewChild('instance') instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();

  focusProduct$ = new Subject<string>();
  clickProduct$ = new Subject<string>();

  focusSubProduct$ = new Subject<string>();
  clickSubProduct$ = new Subject<string>();

  searchCustomers = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focus$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? this.dimentionalService.customers
        : this.dimentionalService.customers.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }

  searchProducts = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.clickProduct$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focusProduct$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? this.dimentionalService.products
        : this.dimentionalService.products.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }

  searchSubProducts = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.clickSubProduct$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focusSubProduct$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? this.dimentionalService.subProducts
        : this.dimentionalService.subProducts.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }


  addQualityRecord() {
    this.saving = true;
    this.qualityTablePayload.Plant_ID = localStorage.getItem('plantId');
    this.qualityTablePayload.Benchmark_ID = this.dimentionalService.benchArray["Quality"]
    this.qualityTablePayload.Prod_ID = this.dimentionalService.getIDFromString(this.qualityTablePayload.Prod_ID)
    this.qualityTablePayload.Sub_Prod_ID = this.dimentionalService.getIDFromString(this.qualityTablePayload.Sub_Prod_ID)
    this.qualityTablePayload.Cust_ID = this.dimentionalService.getIDFromString(this.qualityTablePayload.Cust_ID)
    this.qualityTablePayload.User_ID = JSON.parse(localStorage.getItem('user')).User_ID;
    this.qualityTablePayload.Created_Date = this.dimentionalService.createdDate;
    //this.qualityTablePayload.Updated_Date = moment().format("YYYY\-MM\-DD");
    this.dimentionalService.customerData.push(this.qualityTablePayload);
    //console.log("type", typeof this.qualityTablePayload)
    setTimeout(() => {
      this.activeModal.dismiss('Timeout');
    }, 1000)
    console.log(this.qualityTablePayload);
  }
  
  /** WET code - see the addQualityRecord and change both addQualityRecord and updateQualityRecord */
  updateQualityRecord(){
    this.saving = true;
    this.qualityTablePayload.Plant_ID = localStorage.getItem('plantId');
    this.qualityTablePayload.Benchmark_ID = this.dimentionalService.benchArray["Quality"]
    this.qualityTablePayload.Prod_ID = this.dimentionalService.getIDFromString(this.qualityTablePayload.Prod_ID)
    this.qualityTablePayload.Sub_Prod_ID = this.dimentionalService.getIDFromString(this.qualityTablePayload.Sub_Prod_ID)
    this.qualityTablePayload.Cust_ID = this.dimentionalService.getIDFromString(this.qualityTablePayload.Cust_ID)
    this.qualityTablePayload.User_ID = JSON.parse(localStorage.getItem('user')).User_ID;
    this.qualityTablePayload.Created_Date = this.dimentionalService.createdDate;
    //this.qualityTablePayload.Updated_Date = moment().format("YYYY\-MM\-DD");
    this.dimentionalService.customerData[this.dimentionalService.index] = this.qualityTablePayload;
    //console.log("type", typeof this.qualityTablePayload)
    setTimeout(() => {
      this.activeModal.dismiss('Timeout');
    }, 1000)
    console.log(this.qualityTablePayload);
  }

  loadData(index){
  
    if (index == -1){
      return;
    }
    // console.log("qualit payload ",this.qualityTablePayload);
    let qualityReport = this.dimentionalService.customerData[index];
    console.log(this.dimentionalService.customerData);
    console.log(index);
    this.qualityTablePayload.Cust_ID = this.dimentionalService.customerLookup[qualityReport.Cust_ID];
    this.qualityTablePayload.Prod_ID = this.dimentionalService.productLookup[qualityReport.Prod_ID];
    this.qualityTablePayload.Sub_Prod_ID = this.dimentionalService.subProductLookup[qualityReport.Sub_Prod_ID];
    this.qualityTablePayload.Tot_Rjctd_Qty_For_Mon = qualityReport.Tot_Rjctd_Qty_For_Mon;
    this.qualityTablePayload.Shipped_Qty_For_Mon = qualityReport.Shipped_Qty_For_Mon;
    this.qualityTablePayload.Scrap_For_Mon = qualityReport.Scrap_For_Mon;
    this.qualityTablePayload.Revenue_Mon = qualityReport.Revenue_Mon;
    this.qualityTablePayload.No_Of_Complaints = qualityReport.No_Of_Complaints;
  }
  
  
}
