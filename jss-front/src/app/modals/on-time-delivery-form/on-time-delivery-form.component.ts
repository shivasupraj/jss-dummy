import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbActiveModal, NgbModal, NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subject, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';

import { DimentionalService } from '../../services/dimentional.service';
import * as moment from 'moment';

@Component({
  selector: 'app-on-time-delivery-form',
  templateUrl: './on-time-delivery-form.component.html',
  styleUrls: ['./on-time-delivery-form.component.less']
})
export class OnTimeDeliveryFormComponent implements OnInit {

  onTimeDeliveryTablePayload = {
    Plant_ID: null,
    Benchmark_ID: null,
    Prod_ID: null,
    Sub_Prod_ID: null,
    Cust_ID: null,
    User_ID: null,
    Cnf_Prod_Shipped_Mon: null,
    Tot_Prod_Shipped_Mon: null,
    Revenue_Mon: null,
    Tot_Rjctd_Qty_For_Mon: null,
    Shipped_Qty_For_Mon:null,
    Scrap_For_Mon:null,
    No_Of_Complaints:null,
    Created_Date:null,
    Updated_Date:null
  }

  constructor(public activeModal: NgbActiveModal, private dimentionalService: DimentionalService) {
    this.loadData(this.dimentionalService.index);
  }

  ngOnInit() {
  }


  @ViewChild('instance') instance: NgbTypeahead;
  focusProduct$ = new Subject<string>();
  clickProduct$ = new Subject<string>();

  focusCustomer$ = new Subject<string>();
  clickCustomer$ = new Subject<string>();

  searchProducts = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.clickProduct$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focusProduct$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? this.dimentionalService.products
      : this.dimentionalService.products.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }

  searchCustomers = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.clickCustomer$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focusCustomer$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? this.dimentionalService.customers
      : this.dimentionalService.customers.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }

  addOnTimeRecord(){
    this.onTimeDeliveryTablePayload.Plant_ID = localStorage.getItem('plantId');
    this.onTimeDeliveryTablePayload.Benchmark_ID = this.dimentionalService.benchArray["Productivity"]
    this.onTimeDeliveryTablePayload.Prod_ID = this.dimentionalService.getIDFromString(this.onTimeDeliveryTablePayload.Prod_ID)
    this.onTimeDeliveryTablePayload.Cust_ID = this.dimentionalService.getIDFromString(this.onTimeDeliveryTablePayload.Cust_ID)
    this.onTimeDeliveryTablePayload.Sub_Prod_ID = "PRD000" // no category
    this.onTimeDeliveryTablePayload.User_ID = JSON.parse(localStorage.getItem('user')).User_ID;
    this.onTimeDeliveryTablePayload.Created_Date = this.dimentionalService.createdDate;
    //this.onTimeDeliveryTablePayload.Updated_Date = moment().format("YYYY\-MM\-DD");
    this.dimentionalService.onTimeDeliveryData.push(this.onTimeDeliveryTablePayload);
    setTimeout(()=>{
      this.activeModal.dismiss('Timeout');
    }, 1000)
  }

  /** WET code - see the addOnTimeRecord and change both addQualityRecord and updateOnTimeRecord */
  updateOnTimeRecord(){
    this.onTimeDeliveryTablePayload.Plant_ID = localStorage.getItem('plantId');
    this.onTimeDeliveryTablePayload.Benchmark_ID = this.dimentionalService.benchArray["Productivity"]
    this.onTimeDeliveryTablePayload.Prod_ID = this.dimentionalService.getIDFromString(this.onTimeDeliveryTablePayload.Prod_ID)
    this.onTimeDeliveryTablePayload.Cust_ID = this.dimentionalService.getIDFromString(this.onTimeDeliveryTablePayload.Cust_ID)
    this.onTimeDeliveryTablePayload.Sub_Prod_ID = "PRD000" // no category
    this.onTimeDeliveryTablePayload.User_ID = JSON.parse(localStorage.getItem('user')).User_ID;
    this.onTimeDeliveryTablePayload.Created_Date = this.dimentionalService.createdDate;
    //this.onTimeDeliveryTablePayload.Updated_Date = moment().format("YYYY\-MM\-DD");
    this.dimentionalService.onTimeDeliveryData[0] = this.onTimeDeliveryTablePayload;
    setTimeout(()=>{
      this.activeModal.dismiss('Timeout');
    }, 1000)
  }

  loadData(index) {
    if (index == -1){
      return;
    }
    let onTimeDelivery = this.dimentionalService.onTimeDeliveryData[index];
    this.onTimeDeliveryTablePayload.Prod_ID = this.dimentionalService.productLookup[onTimeDelivery.Prod_ID];
    this.onTimeDeliveryTablePayload.Cust_ID = this.dimentionalService.customerLookup[onTimeDelivery.Cust_ID];
    this.onTimeDeliveryTablePayload.Cnf_Prod_Shipped_Mon = onTimeDelivery.Cnf_Prod_Shipped_Mon;
    this.onTimeDeliveryTablePayload.Tot_Prod_Shipped_Mon = onTimeDelivery.Tot_Prod_Shipped_Mon;
  }
}
