import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductivityKpiFormComponent } from './productivity-kpi-form.component';

describe('ProductivityKpiFormComponent', () => {
  let component: ProductivityKpiFormComponent;
  let fixture: ComponentFixture<ProductivityKpiFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductivityKpiFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductivityKpiFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
