import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbActiveModal, NgbModal, NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subject, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';

import { DimentionalService } from '../../services/dimentional.service';
import * as moment from 'moment';

@Component({
  selector: 'app-productivity-kpi-form',
  templateUrl: './productivity-kpi-form.component.html',
  styleUrls: ['./productivity-kpi-form.component.less']
})
export class ProductivityKpiFormComponent implements OnInit {

  constructor(public activeModal: NgbActiveModal, private dimentionalService: DimentionalService) { 
    this.loadData(this.dimentionalService.index);
  }

  ngOnInit() {
  }

  productivityKpiPayload = {
    Plant_ID:null,
    Benchmark_ID:null,
    User_ID:null,
    Prod_ID:null,
    Emp_Left_Mon:null,
    Tot_Emp_First_Day_Mon:null,
    Tot_Emp_Last_Day_Mon:null,
    Tot_Supp_PPM_Qlty:null,
    Cost_Premium_Freight_In_Mon:null,
    Cost_Premium_Freight_Out_Mon:null,
    Revenue_Mon:null,
    Cogs:null,
    Mon_End_Inv_Local_Curr:null,
    Pre_Mon_End_Inv_Local_Curr:null,
    All_Mfg_Cost_PnL:null,
    Overtime_Cost_PnL:null,
    Created_Date:null,
    Updated_Date:null
    /* product: null,
    totEmpFirstDayMonth: null, Tot_Emp_First_Day_Mon
    totEmpLastDayMonth: null, Tot_Emp_Last_Day_Mon
    empLeftinMonth: null, Emp_Left_Mon
    premFrightinCostDollars: null, Cost_Premium_Freight_In_Mon
    premFrightoutCostDollars: null, Cost_Premium_Freight_Out_Mon
    revenueValue: null, Revenue_Mon
    cogs: null, Cogs
    monthEndInvValue: null, Mon_End_Inv_Local_Curr
    prevMonthEndInvValue: null, Pre_Mon_End_Inv_Local_Curr
    mfgCostsValue: null  All_Mfg_Cost_PnL*/ 
  }

  @ViewChild('instance') instance: NgbTypeahead;
  focusProduct$ = new Subject<string>();
  clickProduct$ = new Subject<string>();

  searchProducts = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.clickProduct$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focusProduct$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? this.dimentionalService.products
      : this.dimentionalService.products.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }

  addProductivityKPIRecord(){
    this.productivityKpiPayload.Plant_ID = localStorage.getItem('plantId');
    this.productivityKpiPayload.Benchmark_ID = this.dimentionalService.benchArray["Productivity"]
    this.productivityKpiPayload.Prod_ID = this.dimentionalService.getIDFromString(this.productivityKpiPayload.Prod_ID)
    this.productivityKpiPayload.User_ID = JSON.parse(localStorage.getItem('user')).User_ID;
    this.productivityKpiPayload.Created_Date = this.dimentionalService.createdDate;
    //this.productivityKpiPayload.Updated_Date = moment().format("YYYY\-MM\-DD");
    this.dimentionalService.productivityData.push(this.productivityKpiPayload);
    setTimeout(()=>{
      this.activeModal.dismiss('Timeout');
    }, 1000)
  }

  /** WET code - see the addProductivityKPIRecord and change both addQualityRecord and updateProductivityKPIRecord */
  updateProductivityKPIRecord(){
    this.productivityKpiPayload.Plant_ID = localStorage.getItem('plantId');
    this.productivityKpiPayload.Benchmark_ID = this.dimentionalService.benchArray["Productivity"]
    this.productivityKpiPayload.Prod_ID = this.dimentionalService.getIDFromString(this.productivityKpiPayload.Prod_ID)
    this.productivityKpiPayload.User_ID = JSON.parse(localStorage.getItem('user')).User_ID;
    this.productivityKpiPayload.Created_Date = this.dimentionalService.createdDate;
    //this.productivityKpiPayload.Updated_Date = moment().format("YYYY\-MM\-DD");
    this.dimentionalService.productivityData[0] = this.productivityKpiPayload;
    setTimeout(()=>{
      this.activeModal.dismiss('Timeout');
    }, 1000)
  }

  loadData(index): any{
    if (index == -1){
      return;
    }
    let productivityReport = this.dimentionalService.productivityData[index];
    this.productivityKpiPayload.Prod_ID = this.dimentionalService.productLookup[productivityReport.Prod_ID];
    this.productivityKpiPayload.Tot_Emp_First_Day_Mon = productivityReport.Emp_Left_Mon;
    this.productivityKpiPayload.Tot_Emp_Last_Day_Mon = productivityReport.Tot_Emp_First_Day_Mon;
    this.productivityKpiPayload.Emp_Left_Mon = productivityReport.Tot_Emp_Last_Day_Mon;
    this.productivityKpiPayload.Cost_Premium_Freight_In_Mon = productivityReport.Cost_Premium_Freight_In_Mon;
    this.productivityKpiPayload.Cost_Premium_Freight_Out_Mon = productivityReport.Cost_Premium_Freight_Out_Mon;
    this.productivityKpiPayload.Revenue_Mon = productivityReport.Revenue_Mon;
    this.productivityKpiPayload.Cogs = productivityReport.Cogs;
    this.productivityKpiPayload.Mon_End_Inv_Local_Curr = productivityReport.Mon_End_Inv_Local_Curr;
    this.productivityKpiPayload.Pre_Mon_End_Inv_Local_Curr = productivityReport.Pre_Mon_End_Inv_Local_Curr;
    this.productivityKpiPayload.All_Mfg_Cost_PnL = productivityReport.All_Mfg_Cost_PnL;
    this.productivityKpiPayload.Overtime_Cost_PnL;
  }

}
