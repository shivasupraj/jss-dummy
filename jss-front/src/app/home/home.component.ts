import { Component, OnInit } from '@angular/core';
import { DimentionalService } from '../services/dimentional.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {
  calYears = ['2017', '2018', '2019', '2020', '2021', '2022', '2023', '2024'];
  calMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
  calMonthsObj:any = [];
  plantObj;
  /* calMonthsObj = [
    {
      "month": 'January',
      "recordPresent": true
    },
    {
      "month": 'February',
      "recordPresent": true
    },
    {
      "month": 'March',
      "recordPresent": true
    },
    {
      "month": 'April',
      "recordPresent": true
    },
    {
      "month": 'May',
      "recordPresent": true
    },
    {
      "month": 'June',
      "recordPresent": false
    },
    {
      "month": 'July',
      "recordPresent": true
    },
    {
      "month": 'August',
      "recordPresent": true
    },
    {
      "month": 'September',
      "recordPresent": true
    },
    {
      "month": 'October',
      "recordPresent": false
    },
    {
      "month": 'November',
      "recordPresent": false
    },
    {
      "month": 'December',
      "recordPresent": false
    }
  ]; */


  selectedYear;

  constructor(private dimentionalService: DimentionalService) {
    this.dimentionalService.resetFields();
  }

  ngOnInit() {
    this.selectedYear = new Date().getFullYear();
    this.setPlant()
  }

  checkMonth(year, month){
    if(month > new Date().getMonth() + 1){
      return true
    }else{
      if(this.selectedYear > new Date().getFullYear()){
        return true
      }else return false;
    };
  }

  setDate(year, month) {
    this.dimentionalService.createdDate = year + "-" + (month < 10 ? "0" + month : month) + "-01"
  }

  setPlant() {
    this.dimentionalService.getPlants().subscribe((result) => {
      this.plantObj = result;
      this.dimentionalService.regionName = this.plantObj[0].Region
      this.dimentionalService.plantName = this.plantObj[0].Plant_Name
      this.dimentionalService.plantId = this.plantObj[0].Plant_ID;
      localStorage.setItem('plantId', this.plantObj[0].Plant_ID);
      this.displayReportsAvailability()
    })
  }

  displayReportsAvailability() {
    this.dimentionalService.getReportsPresentForYear(this.selectedYear,this.dimentionalService.plantId).subscribe((reports) => {
      console.log("reports",reports)
      this.calMonthsObj = reports
    })
  }

  checkWhichYear(year) {
    if (year == this.selectedYear) return true;
    else false;
  }

}
