import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';

import { OnTimeDeliveryFormComponent } from '../modals/on-time-delivery-form/on-time-delivery-form.component';
import { ProductivityKpiFormComponent } from '../modals/productivity-kpi-form/productivity-kpi-form.component';
import { QualityFormComponent } from '../modals/quality-form/quality-form.component';
import { SupplierPpmComponent } from '../modals/supplier-ppm/supplier-ppm.component';
import { DeleteConfirmationComponent } from '../modals/delete-confirmation/delete-confirmation.component';

import { DimentionalService } from '../services/dimentional.service';
import * as moment from 'moment';


@Component({
  selector: 'app-new-report',
  templateUrl: './new-report.component.html',
  styleUrls: ['./new-report.component.less']
})
export class NewReportComponent implements OnInit {


  selectedYear = null;
  selectedMonth = null;
  selectedMonthInNum;
  today = new Date();
  isInReview: boolean = false;
  viewOnly: boolean = false;
  editPage: boolean = false;
  createdDate;

  reportObj;

  customerPayloadArray;
  productPayloadArray;
  plantId;

  formStatuses = {
    plantSet: true,
    safety: true,
    onTimeDeliv: true,
    productivityKPI: true
  }

  constructor(private modalService: NgbModal, private activatedRoute: ActivatedRoute, private router: Router, private dimentionalService: DimentionalService, private location: Location) {
    if (this.activatedRoute.snapshot.queryParams.viewOnly === 'true') { this.viewOnly = true };
    this.selectedYear = this.activatedRoute.snapshot.params.year;
    this.selectedMonthInNum = this.activatedRoute.snapshot.params.month;
    this.plantId = localStorage.getItem('plantId');
    this.selectedMonth = moment().month(parseInt(this.selectedMonthInNum) - 1).format('MMMM');
    //this.createdDate = moment().year(this.selectedYear).month(parseInt(this.selectedMonthInNum)-1).day(1).format("YYYY\-MM\-DD")
    //this.dimentionalService.createdDate = this.selectedYear+"-"+(this.selectedMonthInNum<10?"0"+this.selectedMonthInNum:this.selectedMonthInNum)+"-01";
    console.log(this.activatedRoute.snapshot.params.month, this.selectedMonthInNum, this.selectedMonth, this.dimentionalService.createdDate)
    this.displayReports(this.selectedYear, this.selectedMonthInNum, this.plantId)
  }

  ngOnInit() {
    if (this.viewOnly) { this.isInReview = true }
  }

  displayReports(year, month, plantId) {
    var date = year + "-" + (month < 10 ? "0" + month : month) + "-01"
    this.dimentionalService.createdDate = date
    this.dimentionalService.getReportsForMonth(date, plantId).subscribe((reports) => {
      this.reportObj = reports
      console.log(reports);
      this.dimentionalService.customerData = [];
      this.dimentionalService.supplierData = [];
      this.dimentionalService.onTimeDeliveryData = [];
      this.dimentionalService.productivityData = [];
      for (var key in this.reportObj) {
        if (key == "plant" && this.reportObj.plant.length) {
          this.dimentionalService.safetyPrameters = this.reportObj.plant[0];
        }
        else if (key == "customer") {
          var i;
          for (i = 0; i < this.reportObj.customer.length; i++) {
            if (this.reportObj.customer[i].Benchmark_ID == "BM003") { //this.dimentionalService.benchArray["Productivity"]
              this.dimentionalService.onTimeDeliveryData.push(this.reportObj.customer[i])
            }
            else if (this.reportObj.customer[i].Benchmark_ID == "BM002") { //this.dimentionalService.benchArray["Quality"]
              this.dimentionalService.customerData.push(this.reportObj.customer[i])
            }
          }
        }
        else if (key == "product") {
          var i;
          for (i = 0; i < this.reportObj.product.length; i++) {
            if (this.reportObj.product[i].Benchmark_ID == "BM003") {
              this.dimentionalService.productivityData.push(this.reportObj.product[i])
            }
            else if (this.reportObj.product[i].Benchmark_ID == "BM002") {
              this.dimentionalService.supplierData.push(this.reportObj.product[i])
            }
          }
        }
      }
      //console.log("reports", this.reportObj);
      //this.dimentionalService.productivityData.push(this.productivityKpiPayload);
    })
  }

  openModal(modalComponent) {
    switch (modalComponent) {
      case 'OnTimeDeliveryFormComponent':
        var modalRef = this.modalService.open(OnTimeDeliveryFormComponent);
        break;
      case 'ProductivityKpiFormComponent':
        var modalRef = this.modalService.open(ProductivityKpiFormComponent);
        break;
      case 'QualityFormComponent':
        var modalRef = this.modalService.open(QualityFormComponent);
        break;
      case 'SupplierPpmComponent':
        var modalRef = this.modalService.open(SupplierPpmComponent);
        break;
      case 'DeleteConfirmation':
        var modalRef = this.modalService.open(DeleteConfirmationComponent);
        modalRef.componentInstance.date = this.dimentionalService.safetyPrameters.Created_Date;
        modalRef.componentInstance.plantId = this.dimentionalService.plantId;
        break;
    };
  }

  /* checkIfPlantSet() {
    if (this.selectedPlant == "Select Plant") {
      this.formStatuses.plantSet = false;
      return "No Plant Selected";
    }
    else {
      this.formStatuses.plantSet = true;
      return this.selectedPlant
    }
  } */

  /* checkIfPlantSetBoolean() {
    if (this.selectedPlant == "Select Plant") {
      return true
    }
    else {
      return false
    }
  } */

  delete(array, index) {
    if (array.length == 0){
      return;
    }
    array.splice(index, 1)
  }
  /** Checks if old row is in the table still and then deletes it */
  deleteTopIndex(array){
    console.log(array);
    if(array.length > 1){
      array.splice(1, array.length);
    }
  }

  pushForReview() {
    this.isInReview = true;
    window.scrollTo(0, 0);
    this.dimentionalService.safetyPrameters.Plant_ID = localStorage.getItem('plantId');
    this.dimentionalService.safetyPrameters.Benchmark_ID = this.dimentionalService.benchArray["Safety"]
    this.dimentionalService.safetyPrameters.User_ID = JSON.parse(localStorage.getItem('user')).User_ID;
    this.dimentionalService.safetyPrameters.Created_Date = this.dimentionalService.createdDate;
    /* console.log("Safety", this.dimentionalService.safetyPrameters)
    console.log("customer", this.dimentionalService.customerData)
    console.log("supplier", this.dimentionalService.supplierData)
    console.log("OnTimeDelivery", this.dimentionalService.onTimeDeliveryData)
    console.log("productivityData", this.dimentionalService.productivityData) */
  }

  checkFormValidation() {
    if (this.formStatuses.plantSet && this.formStatuses.safety && this.formStatuses.onTimeDeliv && this.formStatuses.productivityKPI) {
      return true
    }
  }

  save() {
    console.log(this.dimentionalService.customerData);
    console.log(this.dimentionalService.onTimeDeliveryData);
    this.customerPayloadArray = this.dimentionalService.customerData.concat(this.dimentionalService.onTimeDeliveryData);
    this.productPayloadArray = this.dimentionalService.supplierData.concat(this.dimentionalService.productivityData);
    console.log(this.customerPayloadArray);
        
    /* Array.prototype.push.apply(this.dimentionalService.customerData, this.dimentionalService.onTimeDeliveryData);*/
    this.dimentionalService.addPlantReport(this.dimentionalService.safetyPrameters).subscribe((result) => {
      console.log("result", result)
    })
    this.dimentionalService.addCustomerReport(this.customerPayloadArray).subscribe((result) => {
      console.log("result", result)
    })
    this.dimentionalService.addProductReport(this.productPayloadArray).subscribe((result) => {
      console.log("result", result)
    })
    console.log("Plant KPI", this.dimentionalService.safetyPrameters)
    console.log("customer KPI", this.customerPayloadArray)
    console.log("supplier KPI", this.productPayloadArray)
    this.router.navigate(['../']);
  }

  update() {
    console.log(this.dimentionalService.customerData);
    console.log(this.dimentionalService.onTimeDeliveryData);
    this.customerPayloadArray = this.dimentionalService.customerData.concat(this.dimentionalService.onTimeDeliveryData);
    this.productPayloadArray = this.dimentionalService.supplierData.concat(this.dimentionalService.productivityData);
    console.log(this.customerPayloadArray);
    
    /* Array.prototype.push.apply(this.dimentionalService.customerData, this.dimentionalService.onTimeDeliveryData);*/
    // this.dimentionalService.updatePlantReport(this.dimentionalService.safetyPrameters).subscribe((result) => {
    //   console.log("result", result)
    // })
    this.dimentionalService.updateCustomerReport(this.customerPayloadArray).subscribe((result) => {
      console.log("result", result)
    })
    // this.dimentionalService.updateProductReport(this.productPayloadArray).subscribe((result) => {
    //   console.log("result", result)
    // })
    // console.log("Plant KPI", this.dimentionalService.safetyPrameters)
    // console.log("customer KPI", this.customerPayloadArray)
    // console.log("supplier KPI", this.productPayloadArray)
    this.router.navigate(['../']);
  }

  sendIndextoService(index){
    this.dimentionalService.index = index;
  }
}
